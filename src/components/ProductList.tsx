import React from 'react';
import styled from 'styled-components';
import { Product } from './Products';
import { ProductListItems } from  './ProductListItems';

const Styles = styled.div`
.product-list {
  padding-top: 2vh;
  border-bottom: 1px solid rgb(237, 234, 233);
}
`;
interface ProductListProps {
  products: Array<Product>
}

export const ProductList: React.FC<ProductListProps> = ({
    products
}): JSX.Element => {
  return (
    <Styles>
      <div className="product-list">
        {
          products && products.map((product) => {
            let productObj: any = product;
            return <ProductListItems key={productObj.id} product={productObj} />
          })
        }
      </div>
    </Styles>
  );
}