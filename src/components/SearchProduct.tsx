import React from 'react';
import styled from 'styled-components';
import search from '../images/search-icon.png';
import reset from '../images/clear-icon.png';

const Styles = styled.div`
  .search-product {
    padding: 2px;
    width: 50vh;
  }
  .search-form {
    display: flex;
    flex-flow: column;
    border-radius: 25px;
    border: 1px solid rgb(237, 234, 233);
    width: auto;
    height: 2vh;
    padding: 0.7vh 0.7vh;
  }
  .label {
    display: flex;
    width: auto;
  }
  .icon {
    display: flex;
    width: 2vh;
    padding-left: 1.5vh;
    padding-right: 1.5vh;
  }
  .form-control {
    display: flex;
    border: none;
    width: 60vh;
    outline: none;
    font-size: 1.5vh;
    color: rgb(142, 142, 142);
  }
  .reset-btn {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
    padding-right: 2vh;
  }
  .reset-btn img {
    width: 1.8vh;
  }
  .reset-btn-none {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
    padding-right: 2vh;
    display: none;
  }
  @media only screen and (max-width: 799px) {
    .search-product {
      padding: 2px;
      width: 40vh;
    }
    .search-form {
      display: flex;
      flex-flow: column;
      border-radius: 25px;
      border: 1px solid rgb(237, 234, 233);
      width: auto;
      height: 2vh;
      padding: 0.7vh 0.7vh;
    }
    .label {
      display: flex;
      width: auto;
    }
    .icon {
      display: flex;
      width: 2vh;
      padding-left: 1.5vh;
      padding-right: 1.5vh;
    }
    .form-control {
      display: flex;
      border: none;
      width: 80vh;
      outline: none;
      font-size: 1.5vh;
      color: rgb(142, 142, 142);
    }
    .reset-btn {
      display: flex;
      background-color: Transparent;
      background-repeat:no-repeat;
      border: none;
      cursor:pointer;
      overflow: hidden;
      outline:none;
      padding-right: 2vh;
    }
    .reset-btn img {
      width: 1.8vh;
    }
    .reset-btn-none {
      background-color: Transparent;
      background-repeat:no-repeat;
      border: none;
      cursor:pointer;
      overflow: hidden;
      outline:none;
      padding-right: 2vh;
      display: none;
    }
  }
`;
interface SearchProductProps {
  serachString: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onSubmit(event: React.FormEvent<HTMLFormElement>): void;
  onClear: () => void;
}

export const SearchProduct: React.FC<SearchProductProps> = ({
  serachString,
  onChange,
  onSubmit,
  onClear
}): JSX.Element => {
  const resetButton = () => {
    const buttonStyle = serachString === '' ? 'reset-btn-none' : 'reset-btn'; 
    return <button className={buttonStyle} onClick={onClear}><img src={reset} alt={"reset"} /></button>
  };
  
  return (
    <Styles>
      <div className='search-product'>
        <form className='search-form' onSubmit={onSubmit}>
          <label className='label' htmlFor="searchProduct">
            <img src={search} className="icon" alt={"search"}/>
            <input
              className="form-control"
              type="input"
              name="searchProduct"
              value={serachString}
              onChange={onChange}
            />
            <input type="submit" hidden/>
            {resetButton()}
          </label>
        </form>
      </div>
    </Styles>
  );
};
