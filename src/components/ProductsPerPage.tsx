import React from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';

const Styles = styled.div`
  .items-per-page {
    border: 1px solid rgb(204, 204, 204);
    border-radius: 4px;
    background-color: rgb(255, 255, 255);
    padding: 0.5vh 0.5vh;
    color: rgb(142, 142, 142);
    font-size: 1.5vh;
    outline: none;
  }
  .items-per-page option:hover {
    background-color: rgb(204, 204, 204);
    color: rgb(255, 255, 255);
  }
  .form-group {
    display: flex;
  }
  @media only screen and (max-width: 799px) {
    .form-group {
      display: flex;
      width: 23vh;
      padding-top: 2vh;
    }
    .items-per-page {
      border: 0.1px solid rgb(204, 204, 204);
      border-radius: 4px;
      background-color: rgb(255, 255, 255);
      padding: 0.4vh 0.5vh;
      color: rgb(142, 142, 142);
      font-size: 1.8vh;
      outline: none;
    }
    .items-per-page option:hover {
      background-color: rgb(204, 204, 204);
      color: rgb(255, 255, 255);
    }
  }
`;

interface ProductsPerPageProps {
  itemsPerPage: number,
  onChange: (event: React.FormEvent<HTMLSelectElement>) => void
}

export const ProductsPerPage: React.FC<ProductsPerPageProps> = ({
  itemsPerPage,
  onChange,
}): JSX.Element => {
  const [t] = useTranslation();

  return (
    <Styles>
    <div className="form-group">
      <select className="items-per-page" value={itemsPerPage} onChange={onChange}>
        <option value="10">10 {t('produtos por página')}</option>
        <option value="15">15 {t('produtos por página')}</option>
        <option value="20">20 {t('produtos por página')}</option>
      </select>
    </div>
    </Styles>
  );
}