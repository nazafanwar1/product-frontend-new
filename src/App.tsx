import React from 'react';
import './App.css';
import Products from './components/Products';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

const App: React.FC = (): JSX.Element => {
  return (
    <Router>
        <Switch>
          <Route path="/" component={Products} />
        </Switch>
      </Router>
  );
}

export default App;
